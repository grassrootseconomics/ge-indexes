# standard imports
import logging
import os
import hashlib
import base64
import json
from urllib.parse import parse_qs
import urllib.request
import http

# external imports
import cic_base.config
from ge_indexes.db.models.name import Name
import vobject

logg = logging.getLogger(__name__)


def argparser_add(argparser):
    argparser.add_argument('--backend-mode', dest='backend_mode', type=str, default='http', help='Backend retrieval mode')
    argparser.add_argument('endpoint', type=str, default='.', help="URI for metadata retrieval")
    return argparser


def config_overrides():
    overrides = cic_base.config.default_arg_overrides
    overrides.update({
    'endpoint': 'META_BACKEND_ENDPOINT',
    })
    return overrides


def init_backend_auth():
    from cic_auth.client.pgp import PGPClientSession


class HTTPMetaErrorHandler(urllib.request.HTTPErrorProcessor):

    def http_error_4xx(self, request, response, code, msg, headers):
        return response

    http_error_401 = http_error_4xx
    http_error_403 = http_error_4xx
    http_error_404 = http_error_4xx


class HTTPFetcher:

    def __init__(self, path, frontend_auth_mode=None, backend_auth_mode=None):
        self.path = path
        self.opener = urllib.request.build_opener(HTTPMetaErrorHandler)


    def get(self, k):
        path = os.path.join(self.path, k)
        r = self.opener.open(path)
        axx = False
        if r not in [403, 401]:
            axx = True
        return (axx, r,)


# TODO: To use this again, an HTTPResponse wrapper is needed to translate file access / not found errors to the http layer
#class FileFetcher:
#
#    def __init__(self, path):
#        self.path = path
#
#
#    def get(self, k):
#        path = os.path.join(self.path, k)
#        f = open(path, 'r')
#        o = json.loads(f.read())
#        f.close()
#        return o


class Processor:

    def __init__(self, fetcher):
        self.fetcher = fetcher

    def process_set(self, session, env):
        if env.get('REQUEST_METHOD') != 'POST':
            return None
        f = env.get('wsgi.input')
        s = f.read().decode('utf-8')
        k = hexToId(s)
        (axx, response) = self.fetcher.get(k)
        if not axx or response.code == 404:
            return response
        s = base64.b64decode(o['vcard'])
        v = vobject.readOne(stream=s.decode('utf-8'), validate=True)
        o = Name.by_address(k, session)
        o.first_name = v.n.value.given
        o.last_name = v.n.value.family
        session.add(o)
        session.commit()


    def process_get(self, session, env):
        if env.get('REQUEST_METHOD') != 'GET':
            return None
        qs = parse_qs(env.get('QUERY_STRING'))
        q = qs.get('q')
        if q  == None:
            return None
        r = Name.get(q[0], session)
        if len(r) > 0:
            (axx, response) = self.fetcher.get(r[0][2]) # get one item to apply access check
            if not axx:
                return response
        return ('application/json', json.dumps(r))


class Providers:

    __once = False
    __fetcher = None
    processor = None

    @staticmethod
    def init(config):
        if Providers.__once:
            return

        if config.get('_BACKEND_MODE') == 'http':
            Providers.__fetcher = HTTPFetcher(config.get('META_BACKEND_ENDPOINT'))
        #elif config.get('_BACKEND_MODE') == 'file':
        #    Providers.__fetcher = FileFetcher(config.get('META_BACKEND_ENDPOINT'))
        else:
            raise NotImplementedError('Backend mode {}'.format(config.get('_BACKEND_MODE')))

        Providers.processor = Processor(Providers.__fetcher)
        Providers.__once = True



def hexToId(s):
    h = hashlib.new('sha256')
    h.update(bytes.fromhex(s))
    h.update(':cic.person'.encode('utf-8'))
    return h.digest().hex()
