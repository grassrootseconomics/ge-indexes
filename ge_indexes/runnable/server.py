# standard imports
import os
import re
import logging
import argparse

# external imports
import confini
import cic_base.argparse
import cic_base.config

# local imports
from ge_indexes.db import dsn_from_config
from ge_indexes.db.models.base import SessionBase
from ge_indexes.runnable.cmd.server import *

logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

script_dir = os.path.realpath(os.path.dirname(__file__))

argparser = cic_base.argparse.create(script_dir, include_args=cic_base.argparse.full_template)
argparser = cic_base.argparse.add(argparser, argparser_add, 'GE index server arguments')
args = cic_base.argparse.parse(argparser, logger=logg)
overrides = config_overrides()
config = cic_base.config.create(args.c, args, env_prefix=args.env_prefix, arg_overrides=overrides)
config.add(args.backend_mode, '_BACKEND_MODE')

cic_base.config.log(config)


dsn = dsn_from_config(config)
SessionBase.connect(dsn, config.true('DATABASE_DEBUG'))

DEFAULT_CUTOFF = 100


# uwsgi application
def application(env, start_response):

    headers = []
    content = b''

    Providers.init(config)
    processor = Providers.processor

    session = SessionBase.create_session()
    for handler in [
            processor.process_set,
            processor.process_get,
            ]:
        r = handler(session, env)
        if r != None:
            if isinstance(r, http.client.HTTPResponse):
                start_response('{} {}'.format(r.code, r.msg), r.headers.items())
                return [r.read()]
            (mime_type, content) = r
            break
    session.close()

    headers.append(('Content-Length', str(len(content))),)
    headers.append(('Access-Control-Allow-Origin', '*',));

    if len(content) == 0:
        headers.append(('Content-Type', 'text/plain, charset=UTF-8',))
        start_response('404 Looked everywhere, sorry', headers)
    else:
        headers.append(('Content-Type', mime_type,))
        start_response('200 OK', headers)

    return [content]
