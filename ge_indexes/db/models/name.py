import datetime

from sqlalchemy import Column, Integer, String, DateTime, or_
from sqlalchemy.ext.declarative import declarative_base

from ge_indexes.db.models.base import SessionBase

Model = declarative_base(name='Model')


class Name(SessionBase):

    __tablename__ = 'name'

    first_name = Column(String)
    last_name = Column(String)
    address = Column(String)
    date_updated = Column(DateTime, default=datetime.datetime.utcnow)


    @staticmethod
    def get(s, session=None):
        localsession = session
        if session == None:
            localsession = SessionBase.create_session

        print('q {}'.format(s))
        q = localsession.query(Name)
        q = q.filter(or_(Name.first_name.ilike('{}%'.format(s)), Name.last_name.ilike('{}%'.format(s))))
        q = q.order_by(Name.last_name.asc(), Name.first_name.asc())

        r = []
        rows = q.all()
        for entry in rows:
            r.append((
                entry.first_name,
                entry.last_name,
                entry.address,
                ))

        if session == None:
            localsession.close()

        return r


    @staticmethod
    def by_address(address, session=None):
        localsession = session
        if session == None:
            localsession = SessionBase.create_session

        q = localsession.query(Name)
        q = q.filter(Name.address==address)
        o = q.first()

        if session == None:
            localsession.close()

        if o == None:
            o = Name(address)

        o.date_updated = datetime.datetime.utcnow()

        return o


    def __init__(self, address):
        self.address = address
