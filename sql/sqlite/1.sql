DROP INDEX IF EXISTS name_idx;
DROP INDEX IF EXISTS name_address_idx;
DROP TABLE IF EXISTS name;

CREATE TABLE IF NOT EXISTS name (
	id integer primary key autoincrement,
	first_name varchar not null,
	last_name varchar not null,
	address varchar not null,
	date_updated timestamp not null
);

CREATE UNIQUE index name_address_idx on name (first_name, last_name, address);
CREATE UNIQUE index name_idx on name (first_name, last_name);
